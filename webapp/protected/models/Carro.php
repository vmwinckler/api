<?php

/**
 * This is the model class for table "teste.carro".
 *
 * The followings are the available columns in table 'teste.carro':
 * @property integer $id
 * @property integer $id_marca
 * @property integer $id_usuario
 * @property string $modelo
 * @property string $ano
 * @property string $foto
 * @property string $valor
 * @property integer $parcelas
 * @property string $valor_parcelado
 * @property string $dtregistro
 *
 * The followings are the available model relations:
 * @property Marca $idMarca
 * @property Usuario $idUsuario
 * @property CarroParcela[] $carroParcelas
 */
class Carro extends CActiveRecord
{

	const TAXA = 0.007;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'teste.carro';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_marca, id_usuario, modelo, ano', 'required'),
			array('id_marca, id_usuario, ano, id_parcela', 'numerical', 'integerOnly'=>true),
			array('valor, valor_parcelado', 'numerical', 'integerOnly'=>false),
			array('modelo', 'length', 'max'=>50),
			array('ano', 'length', 'max'=>4),
			array('valor, valor_parcelado', 'length', 'max'=>16),
			array('foto', 'file', 'types'=>'gif, jpg, jpeg, png', 'allowEmpty'=>true),
			array('foto', 'unsafe'),
			array('id, id_marca, id_usuario, modelo, ano,  valor, id_parcela, valor_parcelado, dtregistro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'marca' => array(self::BELONGS_TO, 'Marca', 'id_marca'),
			'usuario' => array(self::BELONGS_TO, 'Usuario', 'id_usuario'),
			'parcela' => array(self::BELONGS_TO, 'Parcela', 'id_parcela'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_marca' => 'Marca',
			'id_usuario' => 'Usuário',
			'id_parcela' => 'Max. Parcelas',
			'modelo' => 'Modelo',
			'ano' => 'Ano',
			'foto' => 'Foto',
			'valor' => 'Valor',
			'parcela.qtd' => 'Max. Parcelas',
			'valor_parcelado' => 'Total Parcelado',
			'dtregistro' => 'Data de Registro',
			'usuario.usuario' => 'Registrado por'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_marca',$this->id_marca);
		$criteria->compare('id_usuario',$this->id_usuario);
		$criteria->compare('id_parcela',$this->id_parcela);
		$criteria->compare('modelo',$this->modelo,true);
		$criteria->compare('ano',$this->ano,true);
		$criteria->compare('foto',$this->foto,true);
		$criteria->compare('valor',$this->valor,true);
		$criteria->compare('valor_parcelado',$this->valor_parcelado,true);
		$criteria->compare('dtregistro',$this->dtregistro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Carro the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function behaviors()
	{
	    return array(
	        'image' => array(
	            'class' => 'ext.AttachmentBehavior',
	            # Should be a DB field to store path/filename
	            'attribute' => 'foto',
	            # Default image to return if no image path is found in the DB
	            //'fallback_image' => 'images/sample_image.gif',
	            'path' => "images/uploads/:model/:id.:ext",
/*
	            'processors' => array(
	                array(
	                    # Currently GD Image Processor and Imagick Supported
	                    'class' => 'ImagickProcessor',
	                    'method' => 'resize',
	                    'params' => array(
	                        'width' => 310,
	                        'height' => 150,
	                        'keepratio' => true
	                    )
	                )
	            ),
*/
	            'styles' => array(
	                # name => size
	                # use ! if you would like 'keepratio' => false
	                'thumb' => '!100x60',
	            )
	        ),
	    );
	}
}
