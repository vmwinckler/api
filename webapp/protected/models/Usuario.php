<?php

/**
 * This is the model class for table "teste.usuario".
 *
 * The followings are the available columns in table 'teste.usuario':
 * @property integer $id
  * @property string $nome
 * @property string $usuario
 * @property string $email
 * @property string $senha
 * @property boolean $senha_temp
 * @property string $dtregistro
 *
 * The followings are the available model relations:
 * @property Log[] $logs
 * @property Marca[] $marcas
 * @property Carro[] $carros
 */
class Usuario extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'teste.usuario';
	}

	// holds the password confirmation word
    public $senha_confirma;

    //will hold the encrypted password for update actions.
    public $senhaBase;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nome, usuario, email', 'required'),
			array('nome, email', 'length', 'max'=>50),
			array('usuario', 'length', 'max'=>20),
			array('usuario, email', 'unique'),
			array('senha, senha_confirma', 'required', 'on'=>'novo'),
			array('senha, senha_confirma', 'length', 'min'=>6, 'max'=>40),
        	array('senha', 'compare', 'compareAttribute'=>'senha_confirma'),
			array('senha_temp', 'boolean'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nome, usuario, email, senha, senha_temp, dtregistro', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'logs' => array(self::HAS_MANY, 'Log', 'id_usuario'),
			'marcas' => array(self::HAS_MANY, 'Marca', 'id_usuario'),
			'carros' => array(self::HAS_MANY, 'Carro', 'id_usuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nome' => 'Nome',
			'usuario' => 'Usuário',
			'email' => 'Email',
			'senha' => 'Senha',
			'dtregistro' => 'Data de Registro',
			'senha_confirma' => 'Confirma Senha',
		);
	}

	public function beforeSave()
    {
    	if ($this->getIsNewRecord())
        {
            $this->senha = md5($this->senha);
        }
        else if (!empty($this->senha)&&!empty($this->senha_confirma)&&($this->senha===$this->senha_confirma))
        {
            $this->senha = md5($this->senha);
        } else if(empty($this->senha) && empty($this->senha_confirma) && !empty($this->senhaBase)){
            $this->senha=$this->senhaBase;
        }

         return parent::beforeSave();
    }


	 public function afterFind()
    {
        $this->senhaBase = $this->senha;
        $this->senha = null;

        parent::afterFind();
    }

    /*
	public function saveModel($data=array())
    {
            //because the hashes needs to match
            if(!empty($data['senha']) && !empty($data['senha_confirma']))
            {
                $data['senha'] = Yii::app()->user->hashPassword($data['senha_confirma']);
                $data['senha_confirma'] = Yii::app()->user->hashPassword($data['senha']);
            }

            $this->attributes=$data;

            if(!$this->save())
                return CHtml::errorSummary($this);

         return true;
    }
	*/
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nome',$this->nome,true);
		$criteria->compare('usuario',$this->usuario,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('senha',$this->senha,true);
		$criteria->compare('senha_temp',$this->senha_temp);
		$criteria->compare('dtregistro',$this->dtregistro,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Usuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
