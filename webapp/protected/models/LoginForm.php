<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{
	public $username;
	public $password;
	public $email;
	public $rememberMe;

	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			// username and password are required
			array('username, password', 'required'),
			// rememberMe needs to be a boolean
			array('rememberMe', 'boolean'),
			// password needs to be authenticated
			array('password', 'authenticate'),

			array('email', 'email'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'rememberMe'=>'Remember me next time',
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			if(!$this->_identity->authenticate())
				$this->addError('password','Senha incorreta.');
		}
	}

	/**
	 * Logs in the user using the given username and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login()
	{
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->username,$this->password);
			$this->_identity->authenticate();
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}


	public function recover(){
		$record = Usuario::model()->findByAttributes(array('email'=>$this->email));
		if(isset($record->email)){
			$str = Functions::randomString();
			$record->senha = $str;
			$record->senha_confirma = $record->senha;
			$record->senha_temp = true;
			if($record->save()){
				$message = '<html><head><title>Sua senha temporária</title></head><body>Olá '. ucwords(strtolower($record->nome)).'!<br /><br />Sua senha temporária para acesso ao sistema será: <b>'.$str.'</b>.</body></html>';
				Yii::app()->mailer->Host		= 'smtp.gmail.com';
				Yii::app()->mailer->SMTPAuth	= true;
				Yii::app()->mailer->SMTPSecure	= "ssl";
				Yii::app()->mailer->Port 		= 465;
				Yii::app()->mailer->CharSet 	= 'UTF-8';
				Yii::app()->mailer->Username 	= 'noreply@vmwinckler.com.br';
				Yii::app()->mailer->Password 	= 'a1s2d3f4g';
				Yii::app()->mailer->AddAddress($record->email);
				Yii::app()->mailer->IsSMTP();
				Yii::app()->mailer->IsHTML(true);
				Yii::app()->mailer->From = 'noreply@vmwinckler.com.br';
				Yii::app()->mailer->FromName = 'Sistema';
				Yii::app()->mailer->Subject = 'Sua senha temporária';
				Yii::app()->mailer->Body = $message;
				if(Yii::app()->mailer->Send()){
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}
