<div id="content-header">
	<h1>Configuração</h1>
</div>
<div id="breadcrumb">
	<?php echo CHtml::link('<i class="glyphicon glyphicon-home"></i> <span class="text">Principal</span>',array('index'),array('title'=>'Retorne ao Inicio','class'=>'tip-bottom')); ?>
	<?php echo CHtml::link('<span class="text">Configuração</span>',array(),array('class'=>'current')); ?>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-12">
				<div class="widget-box">
				<div class="widget-title"><span class="icon"><i class="glyphicon glyphicon-bookmark"></i></span><h5>Configurações</h5></div>
				<div class="widget-content">
					<div class="row">
						<div class="col-12 center" style="text-align: center;">
							<ul class="quick-actions">
								<li><?php echo CHtml::link('<i class="icon-user"></i>Permissões', array('auth/assignment')); ?></li>
								<li><?php echo CHtml::link('<i class="icon-people"></i>Usuários', array('usuarios/index')); ?></li>
								<li><?php echo CHtml::link('<i class="icon-book"></i>Logs', array('logs/index')); ?></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>