<div id="container">
    <div id="logo">
       <!-- <img src="img/logo.png" alt="" /> -->
    </div>
    <div id="loginbox">
    	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',array('id' => 'loginform'));	?>
			<p>Entre com seu nome de usuário e senha para continuar.</p>
			<div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
				<?php echo $form->textField($model, 'username', array('class' => 'form-control', 'placeholder'=> "Usuário")); ?>
			</div>
			<div class="input-group">
				<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
				<?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder'=>"Senha")); ?>
			</div>
			<hr />
			<div class="form-actions">
			    <div class="pull-left">
			    	<?php echo CHtml::link('Perdeu sua senha?','#recover',array('class'=>'flip-link to-recover')); ?>
			    	<br />
			    </div>
			    <div class="pull-right">
					<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Entrar', 'htmlOptions' => array('class'=>'btn btn-default'))); ?>
				</div>
			</div>
		<?php
			$this->endWidget();
			unset($form);
		?>

		<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm',array('id' => 'recoverform', 'action' => 'recover'));	?>
			<p>Digite seu e-mail para receber uma senha temporária</p>
			<div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                <?php echo $form->textField($model, 'email', array('class' => 'form-control', 'placeholder'=> "Endereço de E-mail")); ?>
            </div>
            <hr />
            <div class="form-actions">
                <div class="pull-left">
                	<?php echo CHtml::link('&laquo; Retorne ao login','#',array('class'=>'flip-link to-login')); ?>
                    <br />
                </div>
                <div class="pull-right">
                	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType' => 'submit', 'label' => 'Recuperar', 'htmlOptions' => array('class'=>'btn btn-default'))); ?>
                </div>
            </div>
		<?php
			$this->endWidget();
			unset($form);
		?>
    </div>
	<?php
	    foreach(Yii::app()->user->getFlashes() as $key => $message) {
	    	echo '<div class="alert alert-' . $key . '" style="margin-top:5px;"><button class="close" data-dismiss="alert">×</button><strong><i class="fa fa-exclamation-triangle"></i></strong> ' . $message . "</div>\n";
		}
	?>
</div>