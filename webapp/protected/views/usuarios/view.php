<div id="content-header">
	<h1>Usuário #<?php echo $model->id.' - '.$model->usuario; ?></h1>

	<?php
		$this->widget(
    		'bootstrap.widgets.TbButtonGroup',
		    array(
		    	'encodeLabel'=>false,
		        'buttons' => array(
		        	array('label' => '<i class="glyphicon glyphicon-plus"></i>', 'url' => array('create'), 'htmlOptions'=>array('title' =>'Adicionar','class'=>'tip-bottom')),
		        	array('label' => '<i class="glyphicon glyphicon-pencil"></i>', 'url' => array('update','id'=>$model->id), 'htmlOptions'=>array('title' =>'Alterar','class'=>'tip-bottom')),
		        	array('label' => '<i class="glyphicon glyphicon-trash"></i>', 'url' => Yii::app()->controller->createUrl("delete", array("id"=>$model->id)), 'htmlOptions'=>array('title' =>'Excluir','class'=>'tip-bottom confirm')),
		            array('label' => '<i class="glyphicon glyphicon-share-alt"></i>', 'url' => array('index'), 'htmlOptions'=>array('title' =>'Voltar','class'=>'tip-bottom')),
		        ),
		    )
		);
	?>
</div>

<div id="breadcrumb">
	<?php echo CHtml::link('<i class="glyphicon glyphicon-home"></i> <span class="text">Principal</span>',array('site/index'),array('title'=>'Retorne ao Inicio','class'=>'tip-bottom')); ?>
	<?php echo CHtml::link('<span class="text">Configuração</span>',array('/site/page','view'=>'config'),array()); ?>
	<?php echo CHtml::link('<span class="text">Usuários</span>',array('index'),array()); ?>
	<?php echo CHtml::link('<span class="text">'.$model->usuario.'</span>',array(),array('class'=>'current')); ?>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="widget-box">
				<div class="widget-title">
					<span class="icon">
						<i class="glyphicon glyphicon-tag"></i>
					</span>
				</div>
				<div class="widget-content nopadding">
					<?php $this->widget('bootstrap.widgets.TbDetailView',array(
						'data'=>$model,
						'attributes'=>array(
								'id',
								'nome',
								'usuario',
								'email',
								array(	'name'	=> 'dtregistro',
							        	'type'	=> 'text',
							    	    'value'	=> date("d/m/Y", strtotime($model->dtregistro)),
							    	),
						),
					)); ?>
				</div>
			</div>
		</div>
	</div>
</div>