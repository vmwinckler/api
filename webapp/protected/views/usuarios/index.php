<div id="content-header">
	<h1>Usuários</h1>

	<?php
		$this->widget(
    		'bootstrap.widgets.TbButtonGroup',
		    array(
		    	'encodeLabel'=>false,
		        'buttons' => array(
		            array('label' => '<i class="glyphicon glyphicon-plus"></i>', 'url' => 'create', 'htmlOptions'=>array('title' =>'Adicionar','class'=>'tip-bottom')),
		            array('label' => '<i class="glyphicon glyphicon-share-alt"></i>', 'url' => array('/site/page','view'=>'config'), 'htmlOptions'=>array('title' =>'Voltar','class'=>'tip-bottom')),
		        ),
		    )
		);
	?>
</div>

<div id="breadcrumb">
	<?php echo CHtml::link('<i class="glyphicon glyphicon-home"></i> <span class="text">Principal</span>',array('site/index'),array('title'=>'Retorne ao Inicio','class'=>'tip-bottom')); ?>
	<?php echo CHtml::link('<span class="text">Configuração</span>',array('/site/page','view'=>'config'),array()); ?>
	<?php echo CHtml::link('<span class="text">Usuários</span>',array(),array('class'=>'current')); ?>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<?php
			    foreach(Yii::app()->user->getFlashes() as $key => $message) {
		        	echo '<div class="alert alert-' . $key . '" style="margin-top:5px;"><button class="close" data-dismiss="alert">×</button><strong><i class="fa fa-exclamation-triangle"></i></strong> ' . $message . "</div>\n";
		    	}
			?>
			<div class="widget-box">
				<div class="widget-title">
						<span class="icon">
							<i class="glyphicon glyphicon-th"></i>
						</span>
						<h5>Usuários</h5>
					</div>
					<div class="widget-content nopadding">
						<?php $this->widget('bootstrap.widgets.TbExtendedGridView',array(
							'fixedHeader' => true,
							'headerOffset' => 40,
							'type'=>'striped condensed hover',
							'dataProvider'=>$model->search(),
							'responsiveTable' => true,
							'template' => "{items}\n{pager}",
							'sortableRows'=>true,
							'enablePagination'=>true,
							'pager' => array(
                				'class' => 'bootstrap.widgets.TbPager',
                				'htmlOptions'=> array(
									'class'=>'pagination',
								)
                			),
							'columns' => array(
								array(
			                        'class'=>'bootstrap.widgets.TbButtonColumn',
			                        'template'=>'{view}&nbsp;&nbsp;{update}&nbsp;&nbsp;{delete}',
			                        'htmlOptions'=> array('style'=>'text-align: center; width: 4%'),
			                        'buttons'=>array(
			                        	'view' => array(
			                        		'label'=>'Visualizar',
		                                	'icon'=>'fa fa-eye',
		                                	'options'=>array('class'=>'tip-bottom'),
		                                 	'url'=>'Yii::app()->controller->createUrl("view", array("id"=>$data->id))',
		                                ),
		                                'update' => array(
		                                	'label'=>'Atualizar',
		                                	'icon'=>'fa fa-pencil',
		                                	'options'=>array('class'=>'tip-bottom'),
		                                 	'url'=>'Yii::app()->controller->createUrl("update", array("id"=>$data->id))',
		                                ),
		                                'delete' => array(
		                                	'label'=>'Excluir',
		                                	'icon'=>'fa fa-trash-o',
		                                	'url'=>'Yii::app()->controller->createUrl("delete", array("id"=>$data->id))',
		                                	'options'=>array('class'=>'tip-bottom confirm'),
		                                ),
			                        ),
				                ),
								array(	'name'				=> 'id',
							        	'type'				=> 'raw',
							    	    'value'				=> '$data->id',
							        	'htmlOptions'		=> array('style'=>'text-align: center; width: 3%'),
							    	),
								array(	'name'				=> 'nome',
							        	'type'				=> 'raw',
							    	    'value'				=> '$data->nome',
							        	'htmlOptions'		=> array('style'=>'text-align: center; width: 20%'),
							    	),
								array(	'name'				=> 'usuario',
							        	'type'				=> 'raw',
							    	    'value'				=> '$data->usuario',
							        	'htmlOptions'		=> array('style'=>'text-align: center; width: 19%'),
							    	),
								array(	'name'				=> 'email',
							        	'type'				=> 'raw',
							    	    'value'				=> '$data->email',
							        	'htmlOptions'		=> array('style'=>'text-align: center; width: 19%'),
							    	),
							    array(	'name'           	=> 'dtregistro',
							        	'type'            	=> 'raw',
							    	    'value'	          	=> 'date("d/m/Y", strtotime($data->dtregistro))',
							        	'htmlOptions'     	=> array('style'=>'text-align: center; width: 20%'),
							    	),
							),
							'htmlOptions'=> array(
								'class'=>'grid-view nopadding',
								)
						)); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>