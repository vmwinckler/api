<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'usuario-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Campos com <span class="required">*</span> são requeridos.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'nome',array('class'=>'form-control input-small','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'usuario',array('class'=>'form-control input-small','maxlength'=>20)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'form-control input-small','maxlength'=>50)); ?>

	<?php echo $form->passwordFieldRow($model, 'senha', array('class' => 'form-control input-small','maxlength'=>40)); ?>

	<?php echo $form->passwordFieldRow($model, 'senha_confirma', array('class' => 'form-control input-small','maxlength'=>40)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'htmlOptions'=>array('class'=>'btn btn-primary btn-small'),
				'label'=>$model->isNewRecord ? 'Criar' : 'Salvar',
			)); ?>
	</div>

<?php $this->endWidget(); ?>
