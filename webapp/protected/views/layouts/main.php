<!DOCTYPE html>
<html lang="<?php echo Yii::app()->getLanguage(); ?>">
	<head>
		<title>Teste API</title>
		<meta charset="<?php echo Yii::app()->charset; ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <?php
        	define("THEMECSS", Yii::app()->theme->baseUrl . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR);
        	define("THEMEJS", Yii::app()->theme->baseUrl . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR);
        	define("THEMEIMG", Yii::app()->theme->baseUrl . DIRECTORY_SEPARATOR . 'img' .DIRECTORY_SEPARATOR);
			define("CORECSS", Yii::app()->baseUrl . DIRECTORY_SEPARATOR . 'css' . DIRECTORY_SEPARATOR);
			define("COREJS", Yii::app()->baseUrl . DIRECTORY_SEPARATOR . 'js' . DIRECTORY_SEPARATOR);
	        Yii::app()->clientScript->registerCSSFile(THEMECSS.'bootstrap.min.css')
	        						->registerCSSFile(THEMECSS.'bootstrap-glyphicons.css');

	        if(!Yii::app()->user->isGuest):
		        Yii::app()->clientScript->registerCSSFile(THEMECSS.'unicorn.main.css')
		        						->registerCSSFile(THEMECSS.'fullcalendar.css')
		        						->registerCSSFile(THEMECSS.'unicorn.grey.css')
		        						->registerCSSFile(CORECSS.'main.css');
		     else:
		     	Yii::app()->clientScript->registerCSSFile(THEMECSS.'unicorn.login.css')
		        						->registerCSSFile(CORECSS.'main.css');
		     endif
        ?>

	</head>
	<body>
		<?php if(!Yii::app()->user->isGuest): ?>
		<div id="header">
			<h1><a href="#"><?php echo CHtml::encode(Yii::app()->name); ?></a></h1>
			<a id="menu-trigger" href="#"><i class="glyphicon glyphicon-align-justify"></i></a>
		</div>
		<div id="user-nav">
            <ul class="btn-group">
            	<li class="btn"><a href="#"><i class="glyphicon glyphicon-user"></i> <span class="text"><?php echo Yii::app()->user->nome; ?></span></a></li>
                <?php if (Yii::app()->user->checkAccess('admin')): ?>
               		<li class="btn"><?php echo CHtml::link('<i class="glyphicon glyphicon-cog"></i> <span class="text">Configuração</span>',array('/site/page','view'=>'config')); ?></li>
            	<?php endif ?>
                <li class="btn"><?php echo CHtml::link('<i class="glyphicon glyphicon-share-alt"></i> <span class="text">Sair</span>',array('/site/logout')); ?></li>
            </ul>
        </div>
        <div id="sidebar">
			<ul>
				<li class="active"><?php echo CHtml::link('<i class="glyphicon glyphicon-home"></i> <span class="text">Dashboard</span>',array('/site/index')); ?></li>
				<li><?php echo CHtml::link('<i class="glyphicon glyphicon-tag"></i> <span class="text">Marcas</span>',array('/marcas/index')); ?></li>
				<li><?php echo CHtml::link('<i class="glyphicon glyphicon-tag"></i> <span class="text">Carros</span>',array('/carros/index')); ?></li>
			</ul>
		</div>
		<?php endif ?>

		<?php echo $content; ?>

		<?php if(!Yii::app()->user->isGuest): ?>
		<div class="row">
			<div id="footer" class="col-12">
			</div>
		</div>
		<?php endif ?>
  		<?php

	        if(!Yii::app()->user->isGuest):
				Yii::app()->clientScript->registerScriptFile(THEMEJS.'excanvas.min.js')
										->registerScriptFile(THEMEJS.'jquery.min.js')
										->registerScriptFile(THEMEJS.'jquery-ui.custom.js')
										->registerCoreScript('jquery')
										->registerScriptFile(THEMEJS.'bootstrap.min.js')
										->registerScriptFile(THEMEJS.'jquery.flot.min.js')
										->registerScriptFile(THEMEJS.'jquery.flot.resize.min.js')
										->registerScriptFile(THEMEJS.'jquery.sparkline.min.js')
										->registerScriptFile(THEMEJS.'fullcalendar.min.js')
										->registerScriptFile(THEMEJS.'jquery.jpanelmenu.min.js')
										->registerScriptFile(THEMEJS.'unicorn.js')
										->registerScriptFile(THEMEJS.'unicorn.dashboard.js')
										->registerScriptFile(COREJS.'jquery.maskMoney.js')
										->registerScriptFile(COREJS.'main.js');
		     else:
					Yii::app()->clientScript->registerScriptFile(THEMEJS.'jquery.min.js')
											->registerCoreScript('jquery')
											->registerScriptFile(THEMEJS.'unicorn.login.js')
											->registerScriptFile(COREJS.'main.js');
		     endif

  		?>
	</body>
</html>
