<div id="content-header">
	<h1>Nova Marca</h1>

	<?php
		$this->widget(
    		'bootstrap.widgets.TbButtonGroup',
		    array(
		    	'encodeLabel'=>false,
		        'buttons' => array(
		            array('label' => '<i class="glyphicon glyphicon-share-alt"></i>', 'url' => array('index'), 'htmlOptions'=>array('title' =>'Voltar','class'=>'tip-bottom')),
		        ),
		    )
		);
	?>
</div>

<div id="breadcrumb">
	<?php echo CHtml::link('<i class="glyphicon glyphicon-home"></i> <span class="text">Principal</span>',array('site/index'),array('title'=>'Retorne ao Inicio','class'=>'tip-bottom')); ?>
	<?php echo CHtml::link('<span class="text">Marcas</span>',array('marcas/index'),array()); ?>
	<?php echo CHtml::link('<span class="text">Nova</span>',array(),array('class'=>'current')); ?>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="widget-box">
				<div class="widget-title">
					<span class="icon">
						<i class="glyphicon glyphicon-plus"></i>
					</span>
					<h5>Nova Marca</h5>
				</div>
				<div class="widget-content">
					<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
				</div>
			</div>
		</div>
	</div>
</div>