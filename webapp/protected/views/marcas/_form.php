<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'marca-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Campos com <span class="required">*</span> são requeridos.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'descricao',array('class'=>'form-control input-small','maxlength'=>50)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'htmlOptions'=>array('class'=>'btn btn-primary btn-small'),
				'label'=>$model->isNewRecord ? 'Criar' : 'Salvar',
			)); ?>
	</div>

<?php $this->endWidget(); ?>
