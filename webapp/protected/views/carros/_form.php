<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'carro-form',
	'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
        'enctype' => 'multipart/form-data'
    )
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php
		echo $form->select2Row($model, 'id_marca',
			array(
				'asDropDownList'=>true,
				'data' => CHtml::listData(Marca::model()->findAll(),'id', 'descricao'),
				'options'=>array(
					'placeholder' => 'Selecione',
				),
				'htmlOptions'=> array(
					'class'=>'form-control input-small',
					'style'=>'width:80%',
				)
			)
		);
	?>

	<?php echo $form->textFieldRow($model,'modelo',array('class'=>'form-control input-small','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'ano',array('class'=>'form-control input-small','maxlength'=>4)); ?>

	<div class="control-group">
		<?php echo $form->labelEx($model,'foto',array('class'=>'control-label')); ?>
		<div class='controls'>
			<?php echo $form->fileField($model,'foto'); ?>
    	    <?php echo $form->error($model,'foto'); ?>
		</div>
	</div>

	<?php

	$disabled = Yii::app()->user->checkAccess('admin') ? false : true;

	echo $form->textFieldRow($model,'valor',array('class'=>'real form-control input-small','maxlength'=>15, 'disabled'=>$disabled));





	?>

	<?php
		echo $form->select2Row($model, 'id_parcela',
			array(
				'asDropDownList'=>true,
				'data' => CHtml::listData(Parcela::model()->findAll(),'id', 'qtd'),
				'options'=>array(
					'placeholder' => 'Selecione',
				),
				'htmlOptions'=> array(
					'class'=>'form-control input-small',
					'style'=>'width:80%',
				)
			)
		);
	?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'htmlOptions'=>array('class'=>'btn btn-primary btn-small'),
				'label'=>$model->isNewRecord ? 'Criar' : 'Salvar',
			)); ?>
	</div>

<?php $this->endWidget(); ?>