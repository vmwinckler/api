<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',

	'name'=>'Teste API',

	'theme'=>'unicorn',
	'defaultController'=>'site',
    'language'=>'pt_br',
    'sourceLanguage'=>'en',
	'timeZone' => 'America/Sao_Paulo',
    'charset'=>'utf-8',

	'preload'=>array('log','bootstrap'),

	'import'=>array(
		'application.models.*',
		'application.components.*',
		'ext.bootstrap.helpers.TbHtml',
		'application.modules.auth.*',
        'application.modules.auth.components.*',
	),

	'modules'=>array(
		'auth' => array(
			'strictMode' => true,
			'userClass' => 'Usuario',
			'userIdColumn' => 'id',
			'userNameColumn' => 'usuario',
			'defaultLayout' => 'application.views.layouts.column1',
		),
		/*'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'102030',
			'ipFilters'=>array('127.0.0.1','::1'),
			'generatorPaths' => array(
	       		'bootstrap.gii',
    		),
		),
		*/
	),

	'components'=>array(
		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>false,
            'caseSensitive'=>false,
			'rules'=>array(
				'' => 'site/index',
				/*'<view:\w+>'=>'site/page',
			   	'<action:\w+>'=>'<action>',
				*/
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:\w+>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',

			),
		),

		'db'=>array(
			'connectionString' => 'pgsql:host=pgsql.winckler.kinghost.net;port=5432;dbname=winckler',
			'username' => 'winckler',
			'password' => 'qazwsx9344',
			'charset' => 'utf8',
			'emulatePrepare' => true,
		),

		'authManager' => array(
			'class'=>'CDbAuthManager',
        	'connectionID'=>'db',
        	'itemTable'=>'teste.AuthItem',
        	'itemChildTable'=>'teste.AuthItemChild',
        	'assignmentTable'=>'teste.AuthAssignment',
            'behaviors' => array(
                'auth' => array(
                    'class' => 'auth.components.AuthBehavior',
                ),
            ),
        ),
		'user'=>array(
			'class' => 'auth.components.AuthWebUser',
			'admins' => array('admin'),
        ),
		'errorHandler'=>array(
			'errorAction'=>'site/error',
		),

		'bootstrap' => array(
    		'class' => 'ext.bootstrap.components.Bootstrap',
			'responsiveCss' => true,
    		'fontAwesomeCss' => true,
    		'enableBootboxJS' => true,
			'minify' => !YII_DEBUG,
    		'packages' => array(
                'bootstrap.css' => array(
                    'basePath' => null,
                    'baseUrl' => 'themes/unicorn',
                ),
                'bootstrap.js' => array(
                    'basePath' => null,
                    'baseUrl' => 'themes/unicorn',
                ),
           	),
		),

		'mailer' => array(
     		'class' => 'ext.mailer.EMailer',
      		'pathViews' => 'application.views.email',
      		'pathLayouts' => 'application.views.email.layouts'
   		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
                array(
                    'class'=>'ext.LogDb',
                    'connectionID'=>'db',
                    'logTableName'=>'teste.log',
                    'enabled'=>true,
                    'levels'=>'auth',
                ),
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		'adminEmail'=>'vini.rs@gmail.com',
	),

);