<div id="content-header">
    <h1><?php echo $this->capitalize($this->getTypeText(true)); ?></h1>

    <?php $this->widget('bootstrap.widgets.TbButtonGroup',
        array(
            'encodeLabel'=>false,
            'htmlOptions' => array('class'=>'pull-right'),
            'buttons' => array(
                array('label' => '<i class="glyphicon glyphicon-pencil"></i>','url' => array('update', 'name'=>$item->name), 'htmlOptions' => array('title' => Yii::t('AuthModule.main', 'Edit'),'class'=>'tip-bottom')),
                array('label' => '<i class="glyphicon glyphicon-trash"></i>','url' => array('delete', 'name'=>$item->name),'htmlOptions' => array('title' =>Yii::t('AuthModule.main', 'Delete'),'class'=>'tip-bottom', 'confirm' => Yii::t('AuthModule.main', 'Are you sure you want to delete this item?'))),
                array('label' => '<i class="glyphicon glyphicon-share-alt"></i>', 'url' => array('index'), 'htmlOptions'=>array('title' =>'Voltar','class'=>'tip-bottom')),
            ),
        )); 
    ?>
</div>

<div id="breadcrumb">
    <?php echo CHtml::link('<i class="glyphicon glyphicon-home"></i> <span class="text">Principal</span>',array('site/index'),array('title'=>'Retorne ao Inicio','class'=>'tip-bottom')); ?>
    <?php echo CHtml::link('<span class="text">Configuração</span>',array('/site/page','view'=>'config'),array()); ?>
    <?php echo CHtml::link('<span class="text">Permissões</span>',array(),array()); ?>
    <?php echo CHtml::link('<span class="text">'.$this->capitalize($this->getTypeText(true)).'</span>',array('index'),array()); ?>
    <?php echo CHtml::link('<span class="text">'.CHtml::encode($item->description).'</span>',array(),array('class'=>'current')); ?>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h3>
              <?php echo CHtml::encode($item->description); ?>
                <small><?php echo $this->getTypeText(); ?></small>
            </h3>
            <?php $this->widget(
                'zii.widgets.CDetailView',
                array(
                    'data' => $item,
                    'attributes' => array(
                        array(
                            'name' => 'name',
                            'label' => Yii::t('AuthModule.main', 'System name'),
                        ),
                        array(
                            'name' => 'description',
                            'label' => Yii::t('AuthModule.main', 'Description'),
                        ),
                    ),
                )
            ); ?>
        </div>
    </div>
    <hr/>
    <div class="row">
        <div class="col-12">
            <h3>
                <?php echo Yii::t('AuthModule.main', 'Ancestors'); ?>
                <small><?php echo Yii::t('AuthModule.main', 'Permissions that inherit this item'); ?></small>
            </h3>

            <?php $this->widget(
                'bootstrap.widgets.TbGridView',
                array(
                    'type' => 'striped condensed hover',
                    'dataProvider' => $ancestorDp,
                    'emptyText' => Yii::t('AuthModule.main', 'This item does not have any ancestors.'),
                    'template' => "{items}",
                    'hideHeader' => true,
                    'columns' => array(
                        array(
                            'class' => 'AuthItemDescriptionColumn',
                            'itemName' => $item->name,
                        ),
                        array(
                            'class' => 'AuthItemTypeColumn',
                            'itemName' => $item->name,
                        ),
                        array(
                            'class' => 'AuthItemRemoveColumn',
                            'itemName' => $item->name,
                        ),
                    ),
                )
            ); ?>
        </div>
        <div class="col-12">
            <h3>
                <?php echo Yii::t('AuthModule.main', 'Descendants'); ?>
                <small><?php echo Yii::t('AuthModule.main', 'Permissions granted by this item'); ?></small>
            </h3>

            <?php $this->widget(
                'bootstrap.widgets.TbGridView',
                array(
                    'type' => 'striped condensed hover',
                    'dataProvider' => $descendantDp,
                    'emptyText' => Yii::t('AuthModule.main', 'This item does not have any descendants.'),
                    'hideHeader' => true,
                    'template' => "{items}",
                    'columns' => array(
                        array(
                            'class' => 'AuthItemDescriptionColumn',
                            'itemName' => $item->name,
                        ),
                        array(
                            'class' => 'AuthItemTypeColumn',
                            'itemName' => $item->name,
                        ),
                        array(
                            'class' => 'AuthItemRemoveColumn',
                            'itemName' => $item->name,
                        ),
                    ),
                )
            ); ?>

        </div>

    </div>

    <div class="row">

        <div class="col-12">

            <?php if (!empty($childOptions)): ?>

                <h4><?php echo Yii::t('AuthModule.main', 'Add child'); ?></h4>

                <?php $form = $this->beginWidget(
                    'bootstrap.widgets.TbActiveForm',
                    array(
                        'type'=>'inline',
                    )
                ); ?>

                <?php echo $form->dropDownListRow($formModel, 'items', $childOptions, array('label' => false)); ?>

                <?php $this->widget('bootstrap.widgets.TbButton',
                    array(
                        'buttonType' => 'submit',
                        'label' => Yii::t('AuthModule.main', 'Add'),
                    )); ?>

                <?php $this->endWidget(); ?>

            <?php endif; ?>

        </div>

    </div>
</div>