<div id="content-header">
    <h1><?php echo $this->capitalize($this->getTypeText(true)); ?> </h1>

    <?php
        $this->widget(
            'bootstrap.widgets.TbButtonGroup',
            array(
                'encodeLabel'=>false,
                'buttons' => array(
                    array('label' => '<i class="glyphicon glyphicon-user"></i>', 'url' => array('assignment/'), 'htmlOptions'=>array('title' =>Yii::t('AuthModule.main', 'Assignments'),'class'=>'tip-bottom')),
                    array('label' => '<i class="glyphicon glyphicon-briefcase"></i>', 'url' => array('role/'), 'htmlOptions'=>array('title' =>$this->capitalize($this->getItemTypeText(CAuthItem::TYPE_ROLE, true)),'class'=>'tip-bottom')),
                    array('label' => '<i class="glyphicon glyphicon-inbox"></i>', 'url' => array('task/'), 'htmlOptions'=>array('title' =>$this->capitalize($this->getItemTypeText(CAuthItem::TYPE_TASK, true)),'class'=>'tip-bottom')),
                    array('label' => '<i class="glyphicon glyphicon-cog"></i>', 'url' => array('operation/'), 'htmlOptions'=>array('title' =>$this->capitalize($this->getItemTypeText(CAuthItem::TYPE_OPERATION, true)),'class'=>'tip-bottom')),
                ),
            )
        );
    ?>
</div>

<div id="breadcrumb">
    <?php echo CHtml::link('<i class="glyphicon glyphicon-home"></i> <span class="text">Principal</span>',array('site/index'),array('title'=>'Retorne ao Inicio','class'=>'tip-bottom')); ?>
    <?php echo CHtml::link('<span class="text">Configuração</span>',array('/site/page','view'=>'config'),array()); ?>
    <?php echo CHtml::link('<span class="text">Permissões</span>',array(),array()); ?>
    <?php echo CHtml::link('<span class="text">'.$this->capitalize($this->getTypeText(true)).'</span>',array(),array('class'=>'current')); ?>
</div>

<div class="container-fluid">
    <div class="row">&nbsp;</div>
    <div class="row">
        <div class="col-12">

<?php
/* @var $this OperationController|TaskController|RoleController */
/* @var $dataProvider AuthItemDataProvider */

/*$this->breadcrumbs = array(
    $this->capitalize($this->getTypeText(true)),
);
*/
?>

<?php $this->widget('bootstrap.widgets.TbButton', array(
        'type' => 'primary',
        'label' => Yii::t('AuthModule.main', 'Add {type}', array('{type}' => $this->getTypeText())),
        'url' => array('create'),
    )); ?>

<?php $this->widget(
    'bootstrap.widgets.TbGridView',
    array(
        'type' => 'striped hover',
        'dataProvider' => $dataProvider,
        'emptyText' => Yii::t('AuthModule.main', 'No {type} found.', array('{type}' => $this->getTypeText(true))),
        'template' => "{items}\n{pager}",
        'columns' => array(
            array(
                'name' => 'name',
                'type' => 'raw',
                'header' => Yii::t('AuthModule.main', 'System name'),
                'htmlOptions' => array('class' => 'item-name-column'),
                'value' => "CHtml::link(\$data->name, array('view', 'name'=>\$data->name))",
            ),
            array(
                'name' => 'description',
                'header' => Yii::t('AuthModule.main', 'Description'),
                'htmlOptions' => array('class' => 'item-description-column'),
            ),
            array(
                'class' => 'bootstrap.widgets.TbButtonColumn',
                'viewButtonLabel' => Yii::t('AuthModule.main', 'View'),
                'viewButtonUrl' => "Yii::app()->controller->createUrl('view', array('name'=>\$data->name))",
                'updateButtonLabel' => Yii::t('AuthModule.main', 'Edit'),
                'updateButtonUrl' => "Yii::app()->controller->createUrl('update', array('name'=>\$data->name))",
                'deleteButtonLabel' => Yii::t('AuthModule.main', 'Delete'),
                'deleteButtonUrl' => "Yii::app()->controller->createUrl('delete', array('name'=>\$data->name))",
                'deleteConfirmation' => Yii::t('AuthModule.main', 'Are you sure you want to delete this item?'),
            ),
        ),
    )
); ?>
</div>
</div>
</div>
