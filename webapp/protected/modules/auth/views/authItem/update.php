<?php
/* @var $this OperationController|TaskController|RoleController */
/* @var $model AuthItemForm */
/* @var $item CAuthItem */
/* @var $form TbActiveForm */
?>
<div id="content-header">
    <h1>
    <?php echo CHtml::encode($item->description); ?>
    <small><?php echo $this->getTypeText(); ?></small>
    </h1>

    <?php
        $this->widget(
            'bootstrap.widgets.TbButtonGroup',
            array(
                'encodeLabel'=>false,
                'buttons' => array(
                    array('label' => '<i class="glyphicon glyphicon-share-alt"></i>', 'url' => array('index'), 'htmlOptions'=>array('title' =>'Voltar','class'=>'tip-bottom')),
                ),
            )
        );
    ?>
</div>

<div id="breadcrumb">
    <?php echo CHtml::link('<i class="glyphicon glyphicon-home"></i> <span class="text">Principal</span>',array('site/index'),array('title'=>'Retorne ao Inicio','class'=>'tip-bottom')); ?>
    <?php echo CHtml::link('<span class="text">Configuração</span>',array('/site/page','view'=>'config'),array()); ?>
    <?php echo CHtml::link('<span class="text">Permissões</span>',array(),array()); ?>
    <?php echo CHtml::link('<span class="text">'.$this->capitalize($this->getTypeText(true)).'</span>',array(),array()); ?>
    <?php echo CHtml::link('<span class="text">'.Yii::t('AuthModule.main', 'Edit').'</span>',array(),array('class'=>'current')); ?>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="widget-box">
                <div class="widget-title">
                    <span class="icon">
                        <i class="glyphicon glyphicon-plus"></i>
                    </span>
                    <h5><?php echo Yii::t('AuthModule.main', 'Edit'); ?></h5>
                </div>
                <div class="widget-content">
                   <?php $form = $this->beginWidget(
                        'bootstrap.widgets.TbActiveForm',
                        array(
                            'type'=>'horizontal',
                        )
                    ); ?>

                    <?php echo $form->hiddenField($model, 'type'); ?>
                    <?php echo $form->textFieldRow(
                        $model,
                        'name',
                        array(
                            'disabled' => true,
                            'title' => Yii::t('AuthModule.main', 'System name cannot be changed after creation.'),
                        )
                    ); ?>
                    <?php echo $form->textFieldRow($model, 'description'); ?>

                        <div class="form-actions">
                            <?php $this->widget('bootstrap.widgets.TbButton',
                                array(
                                    'buttonType' => 'submit',
                                    'type' => 'primary',
                                    'label' => Yii::t('AuthModule.main', 'Save'),
                                )); ?>
                            <?php $this->widget('bootstrap.widgets.TbButton',
                                array(
                                    'type' => 'link',
                                    'label' => Yii::t('AuthModule.main', 'Cancel'),
                                    'url' => array('view', 'name' => $item->name),
                                )); ?>
                        </div>

                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
    </div>
</div>