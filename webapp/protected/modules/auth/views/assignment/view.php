<div id="content-header">
    <h1><?php echo Yii::t('AuthModule.main', 'Assignments'); ?></h1>

    <?php
        $this->widget(
            'bootstrap.widgets.TbButtonGroup',
            array(
                'encodeLabel'=>false,
                'buttons' => array(
                    array('label' => '<i class="glyphicon glyphicon-share-alt"></i>', 'url' => array('index'), 'htmlOptions'=>array('title' =>'Voltar','class'=>'tip-bottom')),
                ),
            )
        );
    ?>
</div>

<div id="breadcrumb">
    <?php echo CHtml::link('<i class="glyphicon glyphicon-home"></i> <span class="text">Principal</span>',array('site/index'),array('title'=>'Retorne ao Inicio','class'=>'tip-bottom')); ?>
    <?php echo CHtml::link('<span class="text">Configuração</span>',array('/site/page','view'=>'config'),array()); ?>
    <?php echo CHtml::link('<span class="text">Permissões</span>',array(),array()); ?>
    <?php echo CHtml::link('<span class="text">'.Yii::t('AuthModule.main', 'Assignments').'</span>',array('index'),array()); ?>
    <?php echo CHtml::link('<span class="text">'.CHtml::value($model, $this->module->userNameColumn).'</span>',array(),array('class'=>'current')); ?>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h3>
              <?php echo Yii::t('AuthModule.main', 'Permissions'); ?>
                <small><?php echo Yii::t('AuthModule.main', 'Items assigned to this user'); ?></small>
            </h3>
            <?php $this->widget(
            'bootstrap.widgets.TbGridView',
            array(
                'type' => 'striped condensed hover',
                'dataProvider' => $authItemDp,
                'emptyText' => Yii::t('AuthModule.main', 'This user does not have any assignments.'),
                'hideHeader' => true,
                'template' => "{items}",
                'columns' => array(
                    array(
                        'class' => 'AuthItemDescriptionColumn',
                        'active' => true,
                    ),
                    array(
                        'class' => 'AuthItemTypeColumn',
                        'active' => true,
                    ),
                    array(
                        'class' => 'AuthAssignmentRevokeColumn',
                        'userId' => $model->{$this->module->userIdColumn},
                    ),
                ),
            )
        ); ?>

        <?php if (!empty($assignmentOptions)): ?>

            <h4><?php echo Yii::t('AuthModule.main', 'Assign permission'); ?></h4>

            <?php $form = $this->beginWidget(
                'bootstrap.widgets.TbActiveForm',
                array(
                    'type' => 'inline',
                )
            ); ?>

            <?php echo $form->dropDownList($formModel, 'items', $assignmentOptions, array('label' => false)); ?>

            <?php $this->widget('bootstrap.widgets.TbButton', array(
                    'buttonType' => 'submit',
                    'label' => Yii::t('AuthModule.main', 'Assign'),
                )); ?>

            <?php $this->endWidget(); ?>

        <?php endif; ?>

        </div>
    </div>
</div>
