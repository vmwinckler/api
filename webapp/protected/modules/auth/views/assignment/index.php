<div id="content-header">
    <h1><?php echo Yii::t('AuthModule.main', 'Assignments'); ?> </h1>

    <?php
        $this->widget(
            'bootstrap.widgets.TbButtonGroup',
            array(
                'encodeLabel'=>false,
                'buttons' => array(
                    array('label' => '<i class="glyphicon glyphicon-user"></i>', 'url' => array('assignment/'), 'htmlOptions'=>array('title' =>Yii::t('AuthModule.main', 'Assignments'),'class'=>'tip-bottom')),
                    array('label' => '<i class="glyphicon glyphicon-briefcase"></i>', 'url' => array('role/'), 'htmlOptions'=>array('title' =>$this->capitalize($this->getItemTypeText(CAuthItem::TYPE_ROLE, true)),'class'=>'tip-bottom')),
                    array('label' => '<i class="glyphicon glyphicon-inbox"></i>', 'url' => array('task/'), 'htmlOptions'=>array('title' =>$this->capitalize($this->getItemTypeText(CAuthItem::TYPE_TASK, true)),'class'=>'tip-bottom')),
                    array('label' => '<i class="glyphicon glyphicon-cog"></i>', 'url' => array('operation/'), 'htmlOptions'=>array('title' =>$this->capitalize($this->getItemTypeText(CAuthItem::TYPE_OPERATION, true)),'class'=>'tip-bottom')),
                ),
            )
        );
    ?>
</div>
<div id="breadcrumb">
    <?php echo CHtml::link('<i class="glyphicon glyphicon-home"></i> <span class="text">Principal</span>',array('site/index'),array('title'=>'Retorne ao Inicio','class'=>'tip-bottom')); ?>
    <?php echo CHtml::link('<span class="text">Configuração</span>',array('/site/page','view'=>'config'),array()); ?>
    <?php echo CHtml::link('<span class="text">Permissões</span>',array(),array()); ?>
    <?php echo CHtml::link('<span class="text">'.Yii::t('AuthModule.main', 'Assignments').'</span>',array(),array('class'=>'current')); ?>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
        <?php $this->widget('bootstrap.widgets.TbGridView',
            array(
                'type' => 'striped hover',
                'dataProvider' => $dataProvider,
                'emptyText' => Yii::t('AuthModule.main', 'No assignments found.'),
                'template' => "{items}\n{pager}",
                'columns' => array(
                    array(
                        'header' => Yii::t('AuthModule.main', 'User'),
                        'class' => 'AuthAssignmentNameColumn',
                    ),
                    array(
                        'header' => Yii::t('AuthModule.main', 'Assigned items'),
                        'class' => 'AuthAssignmentItemsColumn',
                    ),
                    array(
                        'class' => 'AuthAssignmentViewColumn',
                    ),
                ),
            )
        ); ?>
</div>
</div>
</div>