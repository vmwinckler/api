<?php

class UserIdentity extends CUserIdentity
{
    private $_id;
    public function authenticate()
    {
        $record = Usuario::model()->findByAttributes(array('usuario'=>$this->username));
        if($record===null)
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        else if($record->senhaBase!==md5($this->password))
            $this->errorCode=self::ERROR_PASSWORD_INVALID;
        else
        {
            $this->_id=$record->id;
            $this->setState('nome', $record->nome);
            //$this->setState('id_perfil', $record->id_perfil);
            $this->setState('email', $record->email);
            $this->errorCode=self::ERROR_NONE;
            Yii::log($record->id, 'auth', null);
        }
        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->_id;
    }
}