<?php


class Functions
{

	public static function randomString($length='')
	{
		$str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$max = strlen($str);
		$length = @round($length);
		if(empty($length))
			$length = rand(8,12);
		$string='';
		for($i=0; $i<$length; $i++){
			$string .= $str{rand(0,$max-1)};}
		return $string;
	}


	public static function jurosComposto($valor=0,$taxa=0,$meses=0){
		$montante = $valor * pow((1 + $taxa), $meses);
		$montante = number_format($montante, 2, '.', '');
		$juros = $montante - $valor;
		return $montante;
	}
}