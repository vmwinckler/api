<?php

class LogDb extends CDbLogRoute
{

    protected function createLogTable($db,$tableName)
    {
        $db->createCommand()->createTable($tableName, array(
            'ip'=>'text',
            'id_usuario'=>'integer',
        ));
    }

    protected function processLogs($logs)
    {
        $command=$this->getDbConnection()->createCommand();
        foreach($logs as $log)
        {
            $command->insert($this->logTableName,array(
                'ip'=> Yii::app()->request->userHostAddress,
                'id_usuario'=>(int)$log[0],
            ));
        }
    }

}

?>