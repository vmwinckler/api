-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- PostgreSQL version: 9.3
-- Project Site: pgmodeler.com.br
-- Model Author: ---

SET check_function_bodies = false;
-- ddl-end --


-- Database creation must be done outside an multicommand file.
-- These commands were put in this file only for convenience.
-- -- object: api_teste | type: DATABASE --
-- CREATE DATABASE api_teste
-- 	TABLESPACE = pg_default
-- ;
-- -- ddl-end --
--

-- object: teste | type: SCHEMA --
CREATE SCHEMA teste;
-- ddl-end --

SET search_path TO pg_catalog,public,teste;
-- ddl-end --

-- object: teste.log | type: TABLE --
CREATE TABLE teste.log(
	id serial NOT NULL,
	id_usuario integer NOT NULL,
	ip text NOT NULL,
	dtregistro date NOT NULL DEFAULT now(),
	CONSTRAINT log_pk_id PRIMARY KEY (id)
	WITH (FILLFACTOR = 10)
	USING INDEX TABLESPACE pg_default

)
TABLESPACE pg_default;
-- ddl-end --
-- object: teste.marca | type: TABLE --
CREATE TABLE teste.marca(
	id serial NOT NULL,
	id_usuario integer NOT NULL,
	descricao character varying(50) NOT NULL,
	dtregistro date NOT NULL DEFAULT now(),
	CONSTRAINT marca_pk_id PRIMARY KEY (id)
	WITH (FILLFACTOR = 10)
	USING INDEX TABLESPACE pg_default,
	CONSTRAINT marca_uni_descricao UNIQUE (descricao)

)
TABLESPACE pg_default;
-- ddl-end --
-- object: teste.usuario | type: TABLE --
CREATE TABLE teste.usuario(
	id serial NOT NULL,
	nome character varying(50) NOT NULL,
	usuario character varying(20) NOT NULL,
	email character varying(50) NOT NULL,
	senha text NOT NULL,
	senha_temp boolean NOT NULL DEFAULT false,
	dtregistro date NOT NULL DEFAULT now(),
	CONSTRAINT usuario_pk_id PRIMARY KEY (id)
	WITH (FILLFACTOR = 10)
	USING INDEX TABLESPACE pg_default,
	CONSTRAINT usuario_uni_usuario UNIQUE (usuario)
	WITH (FILLFACTOR = 100)
	USING INDEX TABLESPACE pg_default,
	CONSTRAINT usuario_uni_email UNIQUE (email)
	WITH (FILLFACTOR = 10)
	USING INDEX TABLESPACE pg_default

)
TABLESPACE pg_default;
-- ddl-end --
-- Appended SQL commands --
INSERT INTO teste.usuario (nome,usuario,email,senha) VALUES ('Administrador','admin','vini.rs@gmail.com','4badaee57fed5610012a296273158f5f');
INSERT INTO teste.usuario (nome,usuario,email,senha) VALUES ('Tester','tester','contato@vmwinckler.com.br','4badaee57fed5610012a296273158f5f');
-- ddl-end --

-- object: teste.carro | type: TABLE --
CREATE TABLE teste.carro(
	id serial NOT NULL,
	id_marca integer NOT NULL,
	id_usuario integer NOT NULL,
	id_parcela integer,
	modelo character varying(50) NOT NULL,
	ano numeric(4) NOT NULL,
	foto text,
	valor decimal(21,2),
	valor_parcelado decimal(21,2),
	dtregistro date NOT NULL DEFAULT now(),
	CONSTRAINT carro_pk_id PRIMARY KEY (id)
	WITH (FILLFACTOR = 10)
	USING INDEX TABLESPACE pg_default

)
TABLESPACE pg_default;
-- ddl-end --
-- object: teste.parcela | type: TABLE --
CREATE TABLE teste.parcela(
	id serial NOT NULL,
	qtd integer NOT NULL,
	dtregistro date NOT NULL DEFAULT now(),
	CONSTRAINT parcelas_pk_id PRIMARY KEY (id),
	CONSTRAINT parcela_uni_qtd UNIQUE (qtd)

)
TABLESPACE pg_default;
-- ddl-end --
-- Appended SQL commands --
INSERT INTO teste.parcela (qtd) VALUES (3);
INSERT INTO teste.parcela (qtd) VALUES (6);
INSERT INTO teste.parcela (qtd) VALUES (12);
-- ddl-end --

-- object: log_fk_id_usuario | type: CONSTRAINT --
ALTER TABLE teste.log ADD CONSTRAINT log_fk_id_usuario FOREIGN KEY (id_usuario)
REFERENCES teste.usuario (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: marca_fk_id_usuario | type: CONSTRAINT --
ALTER TABLE teste.marca ADD CONSTRAINT marca_fk_id_usuario FOREIGN KEY (id_usuario)
REFERENCES teste.usuario (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE RESTRICT NOT DEFERRABLE;
-- ddl-end --


-- object: carro_fk_id_marca | type: CONSTRAINT --
ALTER TABLE teste.carro ADD CONSTRAINT carro_fk_id_marca FOREIGN KEY (id_marca)
REFERENCES teste.marca (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: carro_fk_id_usuario | type: CONSTRAINT --
ALTER TABLE teste.carro ADD CONSTRAINT carro_fk_id_usuario FOREIGN KEY (id_usuario)
REFERENCES teste.usuario (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --


-- object: carro_fk_id_parcela | type: CONSTRAINT --
ALTER TABLE teste.carro ADD CONSTRAINT carro_fk_id_parcela FOREIGN KEY (id_parcela)
REFERENCES teste.parcela (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE NOT DEFERRABLE;
-- ddl-end --

create table teste.AuthItem
(
   "name"                 varchar(64) not null,
   "type"                 integer not null,
   "description"          text,
   "bizrule"              text,
   "data"                 text,
   primary key ("name")
);

create table teste.AuthItemChild
(
   "parent"               varchar(64) not null,
   "child"                varchar(64) not null,
   primary key ("parent","child"),
   foreign key ("parent") references "AuthItem" ("name") on delete cascade on update cascade,
   foreign key ("child") references "AuthItem" ("name") on delete cascade on update cascade
);

create table teste.AuthAssignment
(
   "itemname"             varchar(64) not null,
   "userid"               varchar(64) not null,
   "bizrule"              text,
   "data"                 text,
   primary key ("itemname","userid"),
   foreign key ("itemname") references "AuthItem" ("name") on delete cascade on update cascade
);

INSERT INTO teste.AuthItem VALUES ('usuarios.*', 0, 'Controle de Usuários', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('marcas.*', 0, 'Controle de Marcas', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('carros.*', 0, 'Controle de Carros', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('log.*', 0, 'Controle de Logs', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('carros.index', 0, 'Visualizar Carros', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('carros.view', 0, 'Visualizar um Carro', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('marcas.index', 0, 'Visualizar Marcas', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('marcas.view', 0, 'Visualizar uma Marca', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('admin', 2, 'Administrador', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('func', 2, 'Funcionário', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('carros.update', 0, 'Atualizar Carro', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('carros.create', 0, 'Adicionar Carros', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('carros.delete', 0, 'Excluir Carros', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('marcas.delete', 0, 'Excluir Marcas', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('marcas.update', 0, 'Atualizar Marca', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('marcas.create', 0, 'Adicionar Marcas', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('usuarios.delete', 0, 'Excluir Usuários', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('usuarios.create', 0, 'Adicionar Usuários', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('usuarios.index', 0, 'Visualizar Usuários', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('usuarios.update', 0, 'Atualizar Usuário', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('usuarios.view', 0, 'Visualizar um Usuário', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('admUsuarios', 1, 'Administrador de Usuários', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('admMarcas', 1, 'Administrador de Marcas', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('admCarros', 1, 'Administrador de Carros', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('log.index', 0, 'Visualizar Logs', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('admLogs', 1, 'Administrador de Logs', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('viewUsuarios', 1, 'Visualização de Usuários', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('viewCarros', 1, 'Visualizador de Carros', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('viewMarcas', 1, 'Visualizador de Marcas', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('manutUsuarios', 1, 'Manutenção de Usuários', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('manutCarros', 1, 'Manutenção de Carros', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('manutMarcas', 1, 'Manutenção de Marcas', NULL, 'N;');
INSERT INTO teste.AuthItem VALUES ('viewLogs', 1, 'Visualizador de Logs', NULL, 'N;');

INSERT INTO teste.AuthAssignment VALUES ('admin', '1', NULL, 'N;');
INSERT INTO teste.AuthAssignment VALUES ('func', '2', NULL, 'N;');

INSERT INTO teste.AuthItemChild VALUES ('admUsuarios', 'usuarios.view');
INSERT INTO teste.AuthItemChild VALUES ('admUsuarios', 'usuarios.index');
INSERT INTO teste.AuthItemChild VALUES ('admUsuarios', 'usuarios.update');
INSERT INTO teste.AuthItemChild VALUES ('admUsuarios', 'usuarios.create');
INSERT INTO teste.AuthItemChild VALUES ('admUsuarios', 'usuarios.delete');
INSERT INTO teste.AuthItemChild VALUES ('admMarcas', 'marcas.create');
INSERT INTO teste.AuthItemChild VALUES ('admMarcas', 'marcas.update');
INSERT INTO teste.AuthItemChild VALUES ('admMarcas', 'marcas.delete');
INSERT INTO teste.AuthItemChild VALUES ('admMarcas', 'marcas.view');
INSERT INTO teste.AuthItemChild VALUES ('admMarcas', 'marcas.index');
INSERT INTO teste.AuthItemChild VALUES ('admCarros', 'carros.index');
INSERT INTO teste.AuthItemChild VALUES ('admCarros', 'carros.view');
INSERT INTO teste.AuthItemChild VALUES ('admCarros', 'carros.update');
INSERT INTO teste.AuthItemChild VALUES ('admCarros', 'carros.create');
INSERT INTO teste.AuthItemChild VALUES ('admCarros', 'carros.delete');
INSERT INTO teste.AuthItemChild VALUES ('admLogs', 'log.index');
INSERT INTO teste.AuthItemChild VALUES ('viewUsuarios', 'usuarios.view');
INSERT INTO teste.AuthItemChild VALUES ('viewUsuarios', 'usuarios.index');
INSERT INTO teste.AuthItemChild VALUES ('viewCarros', 'carros.view');
INSERT INTO teste.AuthItemChild VALUES ('viewCarros', 'carros.index');
INSERT INTO teste.AuthItemChild VALUES ('viewMarcas', 'marcas.view');
INSERT INTO teste.AuthItemChild VALUES ('viewMarcas', 'marcas.index');
INSERT INTO teste.AuthItemChild VALUES ('admin', 'admUsuarios');
INSERT INTO teste.AuthItemChild VALUES ('admin', 'admMarcas');
INSERT INTO teste.AuthItemChild VALUES ('admin', 'admCarros');
INSERT INTO teste.AuthItemChild VALUES ('admin', 'admLogs');
INSERT INTO teste.AuthItemChild VALUES ('manutUsuarios', 'usuarios.update');
INSERT INTO teste.AuthItemChild VALUES ('manutUsuarios', 'usuarios.create');
INSERT INTO teste.AuthItemChild VALUES ('manutCarros', 'carros.create');
INSERT INTO teste.AuthItemChild VALUES ('manutCarros', 'carros.update');
INSERT INTO teste.AuthItemChild VALUES ('manutMarcas', 'marcas.update');
INSERT INTO teste.AuthItemChild VALUES ('manutMarcas', 'marcas.create');
INSERT INTO teste.AuthItemChild VALUES ('func', 'viewUsuarios');
INSERT INTO teste.AuthItemChild VALUES ('func', 'viewCarros');
INSERT INTO teste.AuthItemChild VALUES ('func', 'viewMarcas');
INSERT INTO teste.AuthItemChild VALUES ('func', 'manutCarros');
INSERT INTO teste.AuthItemChild VALUES ('func', 'manutMarcas');
INSERT INTO teste.AuthItemChild VALUES ('viewLogs', 'log.index');