
$(document).on("click", ".confirm", function(e) {
    var link = $(this).attr("href"); // "get" the intended link in a var
    e.preventDefault();
    bootbox.confirm("Deseja realmente excluir ??", "Não", "Sim", function(confirmed) {
        if (confirmed) {
            document.location.href = link;  // if result, "set" the document location
        }
    });
    $('.modal').css('height','120px');
});



$(document).on("click", ".openThumb", function(e) {
    var link = $(this).attr("href"); // "get" the intended link in a var
    e.preventDefault();
	bootbox.alert("<img src='" + link + "'>", function() {

	});
	$('.modal').css('max-height','455px');
	$('.modal img').css('width','100%');
});

$(document).delegate(".real", 'focus',function(){
    $(this).maskMoney({decimal:",",thousands:"."});
});

